/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.parser;

import testnastroj.object.FoundObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import testnastroj.object.BoundingBox;
import testnastroj.object.Frame;
import testnastroj.object.ObjectAttribute;

//https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
/**
 *
 * @author SaL
 */
public class Parser {

   
    //single object xml
    public static Frame[] parseGroundTruthXMLSingle(String fileName) throws ParserConfigurationException, SAXException, IOException {
        File xml = new File(fileName);
        ArrayList<Frame> frameList = new ArrayList<>();
        String src;
        String className;
        ObjectAttribute[] objAttrs;
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder();
        Document doc = dBuilder.parse(xml);
        NodeList images = doc.getElementsByTagName("image");
        Node image;
        NodeList imageVals;
        Node imageVal;
        NodeList boundingBoxes;
        NodeList objects;
        Node objClass;
        NodeList attrs;
        //prejdi vsetky snimky
        for (int i = 0; i < images.getLength(); i++) {
            image = images.item(i);
            imageVals = image.getChildNodes();
            imageVal = imageVals.item(1); //src
            src = imageVal.getTextContent();
            Node boxes = imageVals.item(5);
            //ak chyba tag atributy, aby to nepadlo
            if (boxes == null) {
                boxes = imageVals.item(3);
            }
            boundingBoxes = boxes.getChildNodes();
            ArrayList<FoundObject> foundList = new ArrayList<>();
            //prejdi vsetky bounding boxy v snimke
            for (int j = 0; j < boundingBoxes.getLength(); j++) {
                if (boundingBoxes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                    objects = boundingBoxes.item(j).getChildNodes();
                    leftTopX = Integer.parseInt(objects.item(1).getTextContent());
                    leftTopY = Integer.parseInt(objects.item(3).getTextContent());
                    width = Integer.parseInt(objects.item(5).getTextContent());
                    height = Integer.parseInt(objects.item(7).getTextContent());
                    className = null; //trieda nemusi byt zadana 
                    objAttrs = null; //a tym padom by neboli ani atributy
                    objClass = objects.item(9);
                    if (objClass != null) {
                        //ak trieda je, este skontroluj atributy
                        if (objClass.hasChildNodes()) {
                            className = objClass.getAttributes().item(0).getTextContent();
                            attrs = objClass.getChildNodes();
                            objAttrs = new ObjectAttribute[(attrs.getLength() - 1) / 2];
                            int atrCount = 0;
                            String atrName;
                            String atrVal;
                            //precitaj jednotlive atributy
                            for (int k = 0; k < attrs.getLength(); k++) {
                                if (attrs.item(k).getNodeType() == Node.ELEMENT_NODE) {
                                    atrName = attrs.item(k).getAttributes().item(0).getTextContent();
                                    atrVal = attrs.item(k).getTextContent();
                                    objAttrs[atrCount++] = new ObjectAttribute(atrName, atrVal);
                                }
                            }
                        }
                    }
                    //pridaj do zoznamu najdenych objektov precitany objekt zo snimku  
                    FoundObject obj = new FoundObject(className, objAttrs);
                    BoundingBox gt = new BoundingBox(leftTopX, leftTopY, width, height);
                    obj.setGroundTruth(gt);
                    foundList.add(obj);
                }
            }
            //pridaj do zoznamu snimok snimku spolu s objektami v nej najdenych
            frameList.add(new Frame(src, foundList.toArray(new FoundObject[foundList.size()])));
        }
        return frameList.toArray(new Frame[frameList.size()]);
    }    
    
    
    //multiple object xml
    public static Frame[] parseGroundTruthXMLMultiple(String fileName) throws ParserConfigurationException, SAXException, IOException {
        File xml = new File(fileName);
        ArrayList<Frame> frameList = new ArrayList<>();
        String src;
        String className;
        ObjectAttribute[] objAttrs;
        int trackId = 0;
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder();
        Document doc = dBuilder.parse(xml);
        NodeList images = doc.getElementsByTagName("image");
        Node image;
        NodeList imageVals;
        Node imageVal;
        NodeList boundingBoxes;
        NodeList objects;
        Node objClass;
        NodeList attrs;
        
        
        //prejdi vsetky snimky
        for (int i = 0; i < images.getLength(); i++) {
            image = images.item(i);
            imageVals = image.getChildNodes();
            imageVal = imageVals.item(1); //src
            src = imageVal.getTextContent();
            Node boxes = imageVals.item(5);
            //ak chyba tag atributy, aby to nepadlo
            if (boxes == null) {
                boxes = imageVals.item(3);
            }
            boundingBoxes = boxes.getChildNodes();
            //ArrayList<FoundObject> foundList = new ArrayList<>();
            ArrayList<FoundObject> foundObj = new ArrayList<>();
            //prejdi vsetky bounding boxy v snimke
            for (int j = 0; j < boundingBoxes.getLength(); j++) {
                if (boundingBoxes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                    objects = boundingBoxes.item(j).getChildNodes();
                    leftTopX = Integer.parseInt(objects.item(1).getTextContent());
                    leftTopY = Integer.parseInt(objects.item(3).getTextContent());
                    width = Integer.parseInt(objects.item(5).getTextContent());
                    height = Integer.parseInt(objects.item(7).getTextContent());
                    className = null; //trieda nemusi byt zadana 
                    objAttrs = null; //a tym padom by neboli ani atributy
                    objClass = objects.item(9);
                    if (objClass != null) {
                        //ak trieda je, este skontroluj atributy
                        if (objClass.hasChildNodes()) {
                            className = objClass.getAttributes().item(0).getTextContent();
                            attrs = objClass.getChildNodes();
                            objAttrs = new ObjectAttribute[((attrs.getLength() - 1) / 2)]; //-1 pre trackId
                            int atrCount = 0;
                            String atrName;
                            String atrVal;
                            //precitaj jednotlive atributy
                            for (int k = 0; k < attrs.getLength(); k++) {
                                if (attrs.item(k).getNodeType() == Node.ELEMENT_NODE) {
                                    atrName = attrs.item(k).getAttributes().item(0).getTextContent();
                                    atrVal = attrs.item(k).getTextContent();
                                    if (atrName.equals("trackId")) {
                                        trackId = Integer.parseInt(atrVal);
                                    } else {
                                        objAttrs[atrCount++] = new ObjectAttribute(atrName, atrVal);
                                    }
                                }
                            }
                        }
                    }
                    //pridaj do zoznamu najdenych objektov precitany objekt zo snimku  
                    FoundObject obj = new FoundObject(trackId, className, objAttrs);
                    BoundingBox gt = new BoundingBox(leftTopX, leftTopY, width, height);
                    obj.setGroundTruth(gt);
                    foundObj.add(obj);       
                }
            }
            Collections.sort(foundObj, new Comparator<FoundObject>() {
                @Override
                public int compare(FoundObject o1, FoundObject o2) {
                    return o1.getTrackId() - o2.getTrackId();
                }
            });
            //pridaj do zoznamu snimok snimku spolu s objektami v nej najdenych
            frameList.add(new Frame(src, foundObj.toArray(new FoundObject[foundObj.size()])));
        }
        return frameList.toArray(new Frame[frameList.size()]);
    }
    
 
    //len pre single object
    public static Frame[] parseGroundTruthTXT(String fileName) throws FileNotFoundException{
        ArrayList<Frame> frameList = new ArrayList<>();
        String src = null;
        String className = null;
        ObjectAttribute[] objAttrs = null;
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        File f = new File(fileName);
        Scanner s = new Scanner(f);
        String[] vals;
        while (s.hasNextLine()) {
            vals = s.nextLine().split(",");
            leftTopX = Integer.parseInt(vals[0]);
            leftTopY = Integer.parseInt(vals[1]);
            width = Integer.parseInt(vals[2]);
            height = Integer.parseInt(vals[3]);
            BoundingBox bb = new BoundingBox(leftTopX, leftTopY, width, height);
            FoundObject obj = new FoundObject(className, objAttrs);
            obj.setGroundTruth(bb);
            FoundObject[] objects = {obj};
            frameList.add(new Frame(src, objects));
        }
        return frameList.toArray(new Frame[frameList.size()]);
    }  
    
    
    /**
     * nacita algo udaje pre multiple object
     * ku vstupnym gt snimkam nacita z xml info o trasovanych objektoch v danych snimkoch
     * @param fileName
     * @param frames snimky nacitane z gt
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public static Frame[] parseAlgoXMLMultiple(String fileName, Frame[] frames) throws ParserConfigurationException, SAXException, IOException {
        File xml = new File(fileName);

        int trackId = 0;
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder();
        Document doc = dBuilder.parse(xml);
        NodeList images = doc.getElementsByTagName("image");
        Node image;
        NodeList imageVals;
        //Node imageVal;
        NodeList boundingBoxes;
        NodeList objects;
        Node objClass;
        NodeList attrs;
        //prejdi vsetky snimky
        for (int i = 0; i < images.getLength(); i++) {
            image = images.item(i);
            imageVals = image.getChildNodes();
            //item(1) = src, item[3] = attributes
            Node boxes = imageVals.item(5);
            //docasne ak chyba tag atributy, aby to nepadlo
            if (boxes == null) {
                boxes = imageVals.item(3);
            }
            boundingBoxes = boxes.getChildNodes();            
            Frame currFrame = frames[i];
            FoundObject[] foundObj = frames[i].getFoundObjects();
            //prejdi vsetky bounding boxy v snimke
            for (int j = 0; j < boundingBoxes.getLength(); j++) {
                if (boundingBoxes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                    objects = boundingBoxes.item(j).getChildNodes();
                    leftTopX = Integer.parseInt(objects.item(1).getTextContent());
                    leftTopY = Integer.parseInt(objects.item(3).getTextContent());
                    width = Integer.parseInt(objects.item(5).getTextContent());
                    height = Integer.parseInt(objects.item(7).getTextContent());
                    
                    objClass = objects.item(9);
                    if (objClass != null) {
                        //ak trieda je, este skontroluj atributy
                        if (objClass.hasChildNodes()) {
                            //className = objClass.getAttributes().item(0).getTextContent();
                            attrs = objClass.getChildNodes();
                            //objAttrs = new ObjectAttribute[((attrs.getLength() - 1) / 2) -1]; //-1 pre trackId
                            //int atrCount = 0;
                            String atrName;
                            String atrVal;
                            //precitaj jednotlive atributy
                            for (int k = 0; k < attrs.getLength(); k++) {
                                if (attrs.item(k).getNodeType() == Node.ELEMENT_NODE) {
                                    atrName = attrs.item(k).getAttributes().item(0).getTextContent();
                                    atrVal = attrs.item(k).getTextContent();
                                    if (atrName.equals("trackId")) {
                                        trackId = Integer.parseInt(atrVal);
                                        break;
                                    } 
                                }
                            }
                        }
                    }
                    
                    //pridaj do zoznamu najdenych objektov precitany objekt zo snimku, sparuj podla trackId  
                    BoundingBox output = new BoundingBox(leftTopX, leftTopY, width, height);
                    for (int k = 0; k < foundObj.length; k++) {
                        if (foundObj[k].getTrackId() == trackId) {
                            foundObj[k].setAlgoValues(output); 
                            break;
                        }
                    }                                       
                }
            }
            //ak sa pre niektory GT nenasla algo hodnota, daj prazdny box
            for (int j = 0; j < foundObj.length; j++) {
                if (foundObj[j].getAlgoValues() == null) {
                    foundObj[j].setAlgoValues(new BoundingBox(0, 0, 0, 0));
                }
            }
            frames[i] = currFrame;
        }
        return frames;
    }
    
    
    //nacita algo udaje pre single object
    public static Frame[] parseAlgoXMLSingle(String fileName, Frame[] frames) throws ParserConfigurationException, SAXException, IOException {
        File xml = new File(fileName);
        //ArrayList<Frame> frameList = new ArrayList<>();
        
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder();
        Document doc = dBuilder.parse(xml);
        NodeList images = doc.getElementsByTagName("image");
        Node image;
        NodeList imageVals;
        //Node imageVal;
        NodeList boundingBoxes;
        NodeList objects;
        //Node objClass;
        //NodeList attrs;
        //prejdi vsetky snimky
        for (int i = 0; i < images.getLength(); i++) {
            image = images.item(i);
            imageVals = image.getChildNodes();
            //item(1) = src, item[3] = attributes
            Node boxes = imageVals.item(5);
            //docasne ak chyba tag atributy, aby to nepadlo
            if (boxes == null) {
                boxes = imageVals.item(3);
            }
            boundingBoxes = boxes.getChildNodes();            
            Frame currFrame = frames[i];
            FoundObject[] foundObj = frames[i].getFoundObjects();
            //prejdi vsetky bounding boxy v snimke
            int foundObjInc = 0;
            for (int j = 0; j < boundingBoxes.getLength(); j++) {
                if (boundingBoxes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                    objects = boundingBoxes.item(j).getChildNodes();
                    leftTopX = Integer.parseInt(objects.item(1).getTextContent());
                    leftTopY = Integer.parseInt(objects.item(3).getTextContent());
                    width = Integer.parseInt(objects.item(5).getTextContent());
                    height = Integer.parseInt(objects.item(7).getTextContent());
                    
                    //pridaj do zoznamu najdenych objektov precitany objekt zo snimku  
                    BoundingBox output = new BoundingBox(leftTopX, leftTopY, width, height);
                    foundObj[foundObjInc++].setAlgoValues(output);                    
                }
            }
            frames[i] = currFrame;
        }
        return frames;
    }
    
  
    
    //len pre single object
    public static Frame[] parseAlgoTXT(String fileName, Frame[] frames) throws FileNotFoundException{
        int leftTopX;
        int leftTopY;
        int width;
        int height;
        File f = new File(fileName);
        Scanner s = new Scanner(f);
        String[] vals;
        int i = 0;
        while (s.hasNextLine()) {
            vals = s.nextLine().split(",");
            leftTopX = Integer.parseInt(vals[0]);
            leftTopY = Integer.parseInt(vals[1]);
            width = Integer.parseInt(vals[2]);
            height = Integer.parseInt(vals[3]);
            BoundingBox bb = new BoundingBox(leftTopX, leftTopY, width, height);
            frames[i++].getFoundObjects()[0].setAlgoValues(bb);
        }
        return frames;
    } 
}
