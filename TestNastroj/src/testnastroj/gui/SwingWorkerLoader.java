/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.SwingWorker;

/**
 *
 * @author Miroslav Buzgo
 */
public class SwingWorkerLoader extends SwingWorker<Icon, Void> {

    private ListModel<ImageView> model;
    private ImageView imageView;
    private final JLabel label;
    
    SwingWorkerLoader(ListModel<ImageView> model,ImageView image,JLabel label){
        this.model = model;
        this.imageView = image;
        this.label = label;
    }
    
    @Override
    protected Icon doInBackground() throws Exception {
        Image image = null;
        try {
            image = ImageIO.read(new File(imageView.getImagePath()));
        } catch (IOException ex) {
            //print error
            Logger.getLogger(ImageView.class.getName()).log(Level.SEVERE, null, ex);
        }
        Image resized = image.getScaledInstance(imageView.getWidth(), imageView.getHeight(), Image.SCALE_DEFAULT);
        return new ImageIcon(resized);
    }

    @Override
    protected void done() {
        try {
            Icon icon = get();
            label.setIcon(icon);
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(SwingWorkerLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    

    
    
    
}
