/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;

import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author
 */
public class CustomCellRenderer extends JLabel
                       implements ListCellRenderer {
    
    public CustomCellRenderer() {
        setOpaque(true);
        setHorizontalAlignment(CENTER);
        setVerticalAlignment(CENTER);
    }

    @Override
    public Component getListCellRendererComponent(JList list,Object value,int index,
                                       boolean isSelected,boolean cellHasFocus) {
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        //ak nema ikonu nacitaj
        if(getIcon() == null){
            ImageIcon icon = ((ImageView)value).getIcon();
            setIcon(icon);
        }
        

        return this;
    }
    }
    
