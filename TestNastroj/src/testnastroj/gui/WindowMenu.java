/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import testnastroj.enums.EnumInputFileType;
import testnastroj.enums.EnumMenuAction;
import testnastroj.enums.EnumObjects;

/**
 *
 * @author
 */
public class WindowMenu implements ActionListener{

    private static WindowMenu instance;

    private JMenu menuFile;
    private final JMenuBar menuBar;
    private JButton buttonSelectGT;
    private JButton buttonSelectAlg;
    private JButton buttonSelectSequence;

    private String gtFilePath;
    private String algFilePath;
    private String sequenceDirectoryPath;
    private JTextField jTextGt;
    private JTextField jTextAlg;
    private JTextField jTextSequence;
    
    private JFileChooser fileChooser;
    
    private final ArrayList<WindowMenuActionListener> listeners;
    private JRadioButton toggleButtonGT;
    private JRadioButton toggleButtonGTXml;
    private JRadioButton toggleButtonAlgXML;
    private JRadioButton toggleButtonAlgTXT;
    private JComboBox<String> comboBox;
    
    private EnumObjects objectType;

    private WindowMenu() {
        menuBar = new JMenuBar();
        
        listeners = new ArrayList<>();
        
        //inicializovat vsetky polozky menu
        initFileMenu();
    }

    public static WindowMenu getInstance() {
        if (instance == null) {
            instance = new WindowMenu();
        }
        return instance;
    }
    
    public void registerListener(WindowMenuActionListener listener){
        listeners.add(listener);
    }
    
    public void unregisterListener(WindowMenuActionListener listener){
        listeners.remove(listener);
    }

    private void initFileMenu() {
        menuFile = new JMenu("File");

        JMenuItem menuItem = new JMenuItem("Open");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //otvorit subor
                fileSelectDialog();
            }
        });
        JMenuItem menuItem2 = new JMenuItem("Save");
        menuItem2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToFile();
            }
        });
        
        JMenuItem menuItem3 = new JMenuItem("Exit");
        menuItem3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //exit
                exitProgram();
            }
        });
        menuFile.add(menuItem);
        menuFile.add(menuItem2);
        menuFile.add(menuItem3);

        menuBar.add(menuFile);
    }

    public JMenuBar getMenu() {
        return menuBar;
    }
    
    private void exitProgram() {
        for (WindowMenuActionListener listener : listeners) {
            listener.onMenuItemSelect(EnumMenuAction.EXIT);
        }
   
    }
    
    private void saveToFile(){
        for (WindowMenuActionListener listener : listeners) {
            listener.onMenuItemSelect(EnumMenuAction.FILE_SAVE);
        }
    }

    private void fileSelectDialog() {
        //reset hodnot suborov
        algFilePath = null;
        gtFilePath = null;
        sequenceDirectoryPath = null;
        
        buttonSelectGT = new JButton("Ground truth file");
        
        toggleButtonGT = new JRadioButton("Text file");
        toggleButtonGT.addActionListener(this);
        toggleButtonGT.setSelected(true);
        toggleButtonGTXml = new JRadioButton("XML file");
        toggleButtonGTXml.addActionListener(this);
        
        comboBox = new JComboBox<String>(new String[]{"Single Object","Multiple Object"});
        comboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    String item = (String) e.getItem();
                    if(item == "Single Object")
                        objectType = EnumObjects.SINGLE;
                    else
                        objectType = EnumObjects.MULTIPLE;
                }
            }
        });
        objectType = EnumObjects.SINGLE;
        
        Box box = Box.createHorizontalBox();
        box.add(toggleButtonGT);
        box.add(toggleButtonGTXml);
        
        buttonSelectAlg = new JButton("Algorithm file");
        buttonSelectSequence = new JButton("Sequence folder");
        this.initFileSelectButtons();

        toggleButtonAlgXML = new JRadioButton("XML file");
        toggleButtonAlgXML.setSelected(true);
        toggleButtonAlgXML.addActionListener(this);
        toggleButtonAlgTXT = new JRadioButton("Text file");
        toggleButtonAlgTXT.addActionListener(this);
        
        Box boxAlg = Box.createHorizontalBox();
        boxAlg.add(toggleButtonAlgTXT);
        boxAlg.add(toggleButtonAlgXML);
        
        jTextGt = new JTextField(15);
        jTextAlg = new JTextField(15);
        jTextSequence = new JTextField(15);

        JComponent[] array = new JComponent[]{comboBox,jTextGt, buttonSelectGT,box, jTextAlg, buttonSelectAlg,boxAlg, jTextSequence, buttonSelectSequence};
        //dialog pre select suborov
        int result = JOptionPane.showConfirmDialog(null, array, "Choose files", JOptionPane.OK_CANCEL_OPTION);
        //ak potvrdil posle sa sprava guicku
        if(result == JOptionPane.OK_OPTION){
            for (WindowMenuActionListener listener : listeners) {
                listener.onMenuItemSelect(EnumMenuAction.FILE_OPEN);
            }
        }
    }

    private void initFileSelectButtons() {
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //historia sa pamata
                if(fileChooser == null){
                    fileChooser = new JFileChooser();
                    fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                }
                int result;
                //vybera sa dictionary
                if (e.getSource().equals(buttonSelectSequence)) {
                    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    result = fileChooser.showOpenDialog(null);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        if (file != null) {
                            sequenceDirectoryPath = file.getPath();
                            jTextSequence.setText(sequenceDirectoryPath);
                        }
                    }
                } else if (e.getSource().equals(buttonSelectAlg) || e.getSource().equals(buttonSelectGT)) {
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    result = fileChooser.showOpenDialog(null);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        if (file != null) {
                            if (e.getSource().equals(buttonSelectAlg)) {
                                algFilePath = file.getPath();
                                jTextAlg.setText(algFilePath);
                            } else {
                                gtFilePath = file.getPath();
                                jTextGt.setText(gtFilePath);
                            }
                        }
                    }
                }

            }
        };

        buttonSelectAlg.addActionListener(listener);
        buttonSelectGT.addActionListener(listener);
        buttonSelectSequence.addActionListener(listener);
    }

    public String getGtAbsolutePath() {
        return gtFilePath;
    }

    public String getAlgAbsolutePath() {
        return algFilePath;
    }

    public String getSequenceDirectoryPath() {
        return sequenceDirectoryPath;
    }
    
    public EnumObjects getObjectType(){
        return objectType;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       if(e.getSource().equals(toggleButtonGT)){
           if(toggleButtonGTXml.isSelected()){
               toggleButtonGTXml.setSelected(false);
               toggleButtonGT.setSelected(true);
           }else{
               toggleButtonGTXml.setSelected(true);
               toggleButtonGT.setSelected(false);
           }
       }else if(e.getSource().equals(toggleButtonGTXml)){
           if(toggleButtonGT.isSelected()){
               toggleButtonGT.setSelected(false);
               toggleButtonGTXml.setSelected(true);
           }else{
               toggleButtonGT.setSelected(true);
               toggleButtonGTXml.setSelected(false);
           }
       }else if(e.getSource().equals(toggleButtonAlgXML)){
           if(toggleButtonAlgTXT.isSelected()){
               toggleButtonAlgTXT.setSelected(false);
               toggleButtonAlgXML.setSelected(true);
           }else{
               toggleButtonAlgTXT.setSelected(true);
               toggleButtonAlgXML.setSelected(false);
           }
       }else if(e.getSource().equals(toggleButtonAlgTXT)){
           if(toggleButtonAlgXML.isSelected()){
               toggleButtonAlgXML.setSelected(false);
               toggleButtonAlgTXT.setSelected(true);
           }else{
               toggleButtonAlgXML.setSelected(true);
               toggleButtonAlgTXT.setSelected(false);
           }
       }
    }
    
    public EnumInputFileType getInputTypeGroundTruth(){
        if(toggleButtonGT != null && toggleButtonGTXml != null){
            if(toggleButtonGT.isSelected())
                return EnumInputFileType.TXT;
            else if(toggleButtonGTXml.isSelected())
                return EnumInputFileType.XML;
        }
        return null;
    }
    
    public EnumInputFileType getInputTypeAlg(){
        if(toggleButtonAlgTXT != null && toggleButtonAlgXML != null){
            if(toggleButtonAlgTXT.isSelected())
                return EnumInputFileType.TXT;
            else if(toggleButtonAlgXML.isSelected())
                return EnumInputFileType.XML;
        }
        return null;
    }
    
    
    interface WindowMenuActionListener{
        /**
         * Ked uzivatel klikne na polozku menu.
         * @param action 
         */
        public void onMenuItemSelect(EnumMenuAction action);
    }

}
