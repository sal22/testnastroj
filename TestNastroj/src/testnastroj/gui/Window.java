/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import testnastroj.chart.ChartWindow;
import testnastroj.enums.EnumIconType;
import testnastroj.enums.EnumInputFileType;
import testnastroj.enums.EnumMenuAction;
import testnastroj.enums.EnumObjects;
import testnastroj.gui.WindowMenu.WindowMenuActionListener;
import testnastroj.metrics.MetricFrame;
import testnastroj.metrics.Metrics;
import testnastroj.object.BoundingBox;
import testnastroj.object.FoundObject;
import testnastroj.object.Frame;
import testnastroj.parser.Parser;

/**
 *
 * @author SaL
 */
public class Window implements WindowMenuActionListener, ComponentListener, ActionListener, MouseWheelListener {

    private final int MIN_SPEED = 5;
    private final int MAX_SPEED = 20;

    private final String WINDOW_NAME = "Test tool for Tracking algorithms evaluation";
    private final JFrame frame;

    private JPanel panelRight;
    private JPanel panelMid;
    private JPanel panelMain;
    private JPanel panelTop;

    private JButton buttonMoveLeft;
    private JButton buttonMoveRight;

    private JTextField inputGroundTruthX;
    private JTextField inputGroundTruthY;
    private JTextField inputGroundTruthWidth;
    private JTextField inputGroundTruthHeight;
    private JTextField inputAlgX;
    private JTextField inputAlgY;
    private JTextField inputAlgWidth;
    private JTextField inputAlgHeight;
    private JTextField inputPrecisionActual;
    private JTextField inputRecallActual;
    private JTextField inputFMeasureActual;
    private JTextField inputIntersectionActual;
    private JTextField inputSubSeqFrom;
    private JTextField inputSubSeqTo;
    private JTextField inputPrecisionSub;
    private JTextField inputRecallSub;
    private JTextField inputFMeasureSub;
    private JTextField inputIntersectionSub;

    private JComboBox<Object> comboBoxMetrics;

    private String[] matrics;
    private JButton buttonGraph;
    private JLabel gtImageLabel;
    private JLabel algImageLabel;
    private JList listSequenceImages;

    private final WindowMenu menu;

    private String gtFilePath;
    private String algFilePath;
    private String sequencePath;

    private int actualFrameIndex;
    private ArrayList<ImageView> listViews;
    private Frame[] frames;
    private Metrics metrics;
    private JLabel labelFrameCount;
    private JButton buttonCalculateSubSeq;
    private JCheckBox toggleButtonBoundingBoxInFrame;
    private boolean sequencePlaying;
    private Thread threadSequence;
    private JButton buttonStartSequence;
    private JButton buttonStopSequence;
    private JSlider sliderSpeed;
    private int wheelRotation;
    private int xGTLabel;
    private int yGTLabel;
    private int xAlgLabel;
    private int yAlgLabel;
    private int xDragGTLabel;
    private int yDragGTLabel;
    private int xDragAlgLabel;
    private int yDragAlgLabel;

    public Window() {
        frame = new JFrame(WINDOW_NAME);
        actualFrameIndex = 0;
        listViews = new ArrayList<>(500);
        //ziska sa screen size
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        //nastavi sa vyska/sirka vzhladom ku velkosti obrazovky
        int width = (int) (5 * screenSize.getWidth() / 6);
        int height = (int) (5 * screenSize.getHeight() / 6);

        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
        //okno sa vytvori na strede obrazovky
        frame.setLocationRelativeTo(null);
        menu = WindowMenu.getInstance();
        //registrovat gui
        menu.registerListener(this);
        frame.setJMenuBar(menu.getMenu());

        matrics = new String[]{"Precision", "Recall", "Intersection over union", "F-Measure"};

        panelMain = new JPanel(new BorderLayout());
        initTopPanel();
        initRightPanel();
        initMidPanel();

        frame.add(panelMain);
        frame.addComponentListener(this);
        frame.revalidate();
        recalculateImageSize();
        
        initMouseAdapter();
    }

    private void initTopPanel() {
        panelTop = new JPanel();
        //label kde bude aktualne cislo snimky / pocet vsetkych
        labelFrameCount = new JLabel();
        toggleButtonBoundingBoxInFrame = new JCheckBox("Enable overlaping");
        buttonStartSequence = new JButton("Start");
        buttonStartSequence.addActionListener(this);
        buttonStopSequence = new JButton("Stop");
        buttonStopSequence.addActionListener(this);

        JLabel labelSpeed = new JLabel("Speed");
        sliderSpeed = new JSlider(SwingConstants.HORIZONTAL, MIN_SPEED, MAX_SPEED, MAX_SPEED);

        panelTop.add(labelFrameCount);
        panelTop.add(toggleButtonBoundingBoxInFrame);
        panelTop.add(buttonStartSequence);
        panelTop.add(buttonStopSequence);
        panelTop.add(labelSpeed);
        panelTop.add(sliderSpeed);

        panelMain.add(panelTop, BorderLayout.NORTH);

    }

    private void initRightPanel() {
        //borders
        Border line = BorderFactory.createLineBorder(Color.BLACK, 1, false);
        Border margin = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        Border outer = BorderFactory.createCompoundBorder(line, margin);

        panelRight = new JPanel(new GridLayout(0, 1));
        panelRight.setBorder(outer);

        //precision pre aktualny snimok
        JPanel panelPrecision = new JPanel(new GridLayout(1, 2));
        panelPrecision.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel labelPrecision = new JLabel("Precision");
        inputPrecisionActual = new JTextField();
        panelPrecision.add(labelPrecision);
        panelPrecision.add(inputPrecisionActual);

        //recall pre aktualny snimok
        JPanel panelRecall = new JPanel(new GridLayout(1, 2));
        panelRecall.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel labelRecall = new JLabel("Recall");
        inputRecallActual = new JTextField();
        panelRecall.add(labelRecall);
        panelRecall.add(inputRecallActual);

        //f-measue aktualny snimok
        JPanel panelFMeasure = new JPanel(new GridLayout(1, 2));
        panelFMeasure.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel labelFMeasure = new JLabel("F-Measure");
        inputFMeasureActual = new JTextField();
        panelFMeasure.add(labelFMeasure);
        panelFMeasure.add(inputFMeasureActual);

        //intersection union pre aktualny snimok
        JPanel panelIntersection = new JPanel(new GridLayout(1, 2));
        panelIntersection.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel labelIntersection = new JLabel("Intersection");
        inputIntersectionActual = new JTextField();
        panelIntersection.add(labelIntersection);
        panelIntersection.add(inputIntersectionActual);

        //pridat do kontajnera panel pre aktualny snimok
        panelRight.add(new JLabel("Current frame"));
        panelRight.add(panelPrecision);
        panelRight.add(panelRecall);
        panelRight.add(panelFMeasure);
        panelRight.add(panelIntersection);

        //panel so vstupom pre subsekvenciu
        JPanel panelSubSeq = new JPanel(new GridLayout(1, 4));
        panelSubSeq.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel labelFrom = new JLabel("From:");
        inputSubSeqFrom = new JTextField();
        JLabel labelTo = new JLabel("To:");
        inputSubSeqTo = new JTextField();
        buttonCalculateSubSeq = new JButton("Recalculate");
        buttonCalculateSubSeq.addActionListener(this);
        panelSubSeq.add(labelFrom);
        panelSubSeq.add(inputSubSeqFrom);
        panelSubSeq.add(labelTo);
        panelSubSeq.add(inputSubSeqTo);

        //precision pre subsekvenciu
        panelPrecision = new JPanel(new GridLayout(1, 2));
        panelPrecision.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        labelPrecision = new JLabel("Precision");
        inputPrecisionSub = new JTextField();
        panelPrecision.add(labelPrecision);
        panelPrecision.add(inputPrecisionSub);

        //recall pre subsekvenciu
        panelRecall = new JPanel(new GridLayout(1, 2));
        panelRecall.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        labelRecall = new JLabel("Recall");
        inputRecallSub = new JTextField();
        panelRecall.add(labelRecall);
        panelRecall.add(inputRecallSub);

        //f-measue pre subsekvenciu
        panelFMeasure = new JPanel(new GridLayout(1, 2));
        panelFMeasure.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        labelFMeasure = new JLabel("F-Measure");
        inputFMeasureSub = new JTextField();
        panelFMeasure.add(labelFMeasure);
        panelFMeasure.add(inputFMeasureSub);

        //intersection union pre subsekvenciu
        panelIntersection = new JPanel(new GridLayout(1, 2));
        panelIntersection.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        labelIntersection = new JLabel("Intersection");
        inputIntersectionSub = new JTextField();
        panelIntersection.add(labelIntersection);
        panelIntersection.add(inputIntersectionSub);

        //spinner
        comboBoxMetrics = new JComboBox<Object>(matrics);
        comboBoxMetrics.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        buttonGraph = new JButton("Show graph");
        buttonGraph.addActionListener(this);
        buttonGraph.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        panelRight.add(new JLabel("Subsequence"));
        panelRight.add(panelSubSeq);
        panelRight.add(buttonCalculateSubSeq);
        panelRight.add(panelPrecision);
        panelRight.add(panelRecall);
        panelRight.add(panelFMeasure);
        panelRight.add(panelIntersection);
        panelRight.add(comboBoxMetrics);
        panelRight.add(buttonGraph);

        panelMain.add(panelRight, BorderLayout.EAST);
    }

    private void initMidPanel() {
        panelMid = new JPanel(new BorderLayout());
        panelMid.setBackground(Color.BLUE);
        this.initSequencePanel();
        this.initImagePanel();

        panelMain.add(panelMid, BorderLayout.CENTER);
    }

    private void initImagePanel() {
        //panel s 2 stlpcami a 1 riadkom v nom bude obrazok a info o objektoch
        JPanel imagePanel = new JPanel(new GridLayout(1, 2));
        //lavy panel s ground truth datami
        JPanel gtPanel = new JPanel();
        gtPanel.setLayout(new BorderLayout());

        //borders
        Border line = BorderFactory.createLineBorder(Color.BLACK, 1, false);
        Border margin = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        Border outer = BorderFactory.createCompoundBorder(line, margin);
        Border bord = BorderFactory.createCompoundBorder(margin, outer);

        JLabel groundTruthText = new JLabel("Ground truth bounding boxes");
        groundTruthText.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel algText = new JLabel("Algorithm bounding boxes");
        algText.setHorizontalAlignment(SwingConstants.CENTER);

        gtImageLabel = new JLabel();
        gtImageLabel.addMouseWheelListener(this);

        gtImageLabel.setBorder(bord);
        gtPanel.add(gtImageLabel, BorderLayout.CENTER);
        gtPanel.add(groundTruthText, BorderLayout.NORTH);

        //info panel s textboxmi
        JPanel infoPanel = new JPanel();
        infoPanel.setBorder(bord);
        infoPanel.setLayout(new GridLayout(2, 4));
        inputGroundTruthX = new JTextField();
        inputGroundTruthY = new JTextField();
        inputGroundTruthWidth = new JTextField();
        inputGroundTruthHeight = new JTextField();
        infoPanel.add(new JLabel("X"));
        infoPanel.add(inputGroundTruthX);
        infoPanel.add(new JLabel("Width"));
        infoPanel.add(inputGroundTruthWidth);
        infoPanel.add(new JLabel("Y"));
        infoPanel.add(inputGroundTruthY);
        infoPanel.add(new JLabel("Height"));
        infoPanel.add(inputGroundTruthHeight);
        //infobox sa prida do gt panelu
        gtPanel.add(infoPanel, BorderLayout.SOUTH);

        
        JPanel algPanel = new JPanel();
        algPanel.setLayout(new BorderLayout());

        algImageLabel = new JLabel();
        algImageLabel.addMouseWheelListener(this);
        algImageLabel.setBorder(bord);
        algPanel.add(algImageLabel, BorderLayout.CENTER);

        infoPanel = new JPanel();
        infoPanel.setBorder(bord);
        infoPanel.setLayout(new GridLayout(2, 4));

        inputAlgX = new JTextField();
        inputAlgY = new JTextField();
        inputAlgWidth = new JTextField();
        inputAlgHeight = new JTextField();
        infoPanel.add(new JLabel("X"));
        infoPanel.add(inputAlgX);
        infoPanel.add(new JLabel("Width"));
        infoPanel.add(inputAlgWidth);
        infoPanel.add(new JLabel("Y"));
        infoPanel.add(inputAlgY);
        infoPanel.add(new JLabel("Height"));
        infoPanel.add(inputAlgHeight);

        algPanel.add(infoPanel, BorderLayout.SOUTH);
        algPanel.add(algText, BorderLayout.NORTH);

        imagePanel.add(gtPanel);
        imagePanel.add(algPanel);

        panelMid.add(imagePanel, BorderLayout.CENTER);
    }

    private void initSequencePanel() {
        JPanel panelTopMid = new JPanel(new BorderLayout());
        buttonMoveLeft = new JButton("<");
        buttonMoveLeft.addActionListener(this);

        buttonMoveRight = new JButton(">");
        buttonMoveRight.addActionListener(this);
        panelTopMid.add(buttonMoveLeft, BorderLayout.WEST);

        listSequenceImages = createImageList(false);
        listSequenceImages.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                ImageView selected = ((JList<ImageView>) e.getSource()).getSelectedValue();
                int index = listViews.indexOf(selected);
                if (index != -1) {
                    actualFrameIndex = index;
                    listSequenceImages.ensureIndexIsVisible(actualFrameIndex);
                    labelFrameCount.setText(actualFrameIndex + 1 + "/" + frames.length);
                    setImageView();
                }

            }
        });

        JScrollPane scroll = new JScrollPane(listSequenceImages);
        panelTopMid.add(scroll, BorderLayout.CENTER);
        panelTopMid.add(buttonMoveRight, BorderLayout.EAST);
        panelMid.add(panelTopMid, BorderLayout.NORTH);
    }

    private JList createImageList(boolean selectedSequence) {
        JList imageList = new JList(createModel(selectedSequence));
        imageList.setCellRenderer(new CustomCellRenderer());
        imageList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        imageList.setVisibleRowCount(1);
        return imageList;
    }

    private DefaultListModel<ImageView> createModel(boolean selectedSequence) {
        DefaultListModel<ImageView> model = new DefaultListModel<>();
        final String[] extensions = ImageFactory.IMAGE_EXTENSIONS;
        if (selectedSequence) {
            File sequenceDir = new File(sequencePath);
            if (sequenceDir.isDirectory()) {
                //iba obrazky
                File[] allFiles = sequenceDir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        for (String extension : extensions) {
                            if (name.endsWith("." + extension)) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
                //zoradit podla nazvu
                Arrays.sort(allFiles);
                listViews.clear();
                for (File file : allFiles) {
                    ImageView view = new ImageView(100, 100, file.getPath());
                    listViews.add(view);
                    model.addElement(view);
                }
            }
        }
        return model;
    }

    @Override
    public void onMenuItemSelect(EnumMenuAction action) {
        switch (action) {
            case FILE_OPEN:
                sequencePath = menu.getSequenceDirectoryPath();
                gtFilePath = menu.getGtAbsolutePath();
                algFilePath = menu.getAlgAbsolutePath();
                EnumInputFileType groundTruthFileType = menu.getInputTypeGroundTruth();
                EnumInputFileType alghorithmFileType = menu.getInputTypeAlg();
                //pri kazdom pokuse o otvorenie suborov cez menu sa tieto stringy nulluju v menu
                //takze sa nestane ze by tam boli hodnoty z predchadzajucej sekvencie
                if (sequencePath != null && gtFilePath != null && algFilePath != null && groundTruthFileType != null) {
                    try {
                        actualFrameIndex = 0;
                        EnumObjects type = menu.getObjectType();
                        if(type == EnumObjects.SINGLE){
                            if(groundTruthFileType == EnumInputFileType.TXT)
                                frames = Parser.parseGroundTruthTXT(gtFilePath);
                            else if (groundTruthFileType == EnumInputFileType.XML) 
                                frames = Parser.parseGroundTruthXMLSingle(gtFilePath);
                            
                            if (alghorithmFileType == EnumInputFileType.XML) 
                                frames = Parser.parseAlgoXMLSingle(algFilePath, frames);
                            else if (alghorithmFileType == EnumInputFileType.TXT) 
                                frames = Parser.parseAlgoTXT(gtFilePath, frames);   
                        }else if(type == EnumObjects.MULTIPLE){
                            //tu sa da iba z XML-ka
                            frames = Parser.parseGroundTruthXMLMultiple(gtFilePath);
                            frames = Parser.parseAlgoXMLMultiple(algFilePath, frames);
                        }
                        
                        
                        

                        metrics = new Metrics(frames);

                        //listSequenceImages.setModel(createInitModel());
                        listSequenceImages.setModel(createModel(true));
                        //prekreslenie panelu musi ist najskor, zmeni to vysku
                        panelMid.validate();
                        //update image pre gt a alg
                        this.setImageView();
                        //nastavenie velkosti sekvencie
                        labelFrameCount.setText(actualFrameIndex + 1 + "/" + frames.length);
                        inputSubSeqFrom.setText(String.valueOf(actualFrameIndex + 1));
                        inputSubSeqTo.setText(String.valueOf(frames.length));
                        updateSubsequentValues();
                    } catch (FileNotFoundException ex) {
                        printErrorMsg("File not found.");
                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        printErrorMsg("Parser error.");
                    }

                } else {
                    printErrorMsg("Choose all files.");
                }
                break;
            case EXIT:
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                break;
            case FILE_SAVE:
                exportToFile();
                break;

        }
    }

    private void setImageView() {
        //nastavi sa aktualne oznaceny obrazok zo sekvencie
        ImageView selected = listViews.get(actualFrameIndex);
        String path = selected.getImagePath();
        int labelWidth = gtImageLabel.getWidth() - gtImageLabel.getInsets().left - gtImageLabel.getInsets().right;
        int labelHeight = gtImageLabel.getHeight() - gtImageLabel.getInsets().top - gtImageLabel.getInsets().bottom;

        if (frames != null) {
            Frame actualF = frames[actualFrameIndex];
            FoundObject[] objects = actualF.getFoundObjects();
            BoundingBox[] algBoxes = new BoundingBox[objects.length];
            BoundingBox[] gtBoxes = new BoundingBox[objects.length];
            //vsetky bounding boxs v danom frame
            for (int i = 0; i < objects.length; i++) {
                algBoxes[i] = objects[i].getAlgoValues();
                gtBoxes[i] = objects[i].getGroundTruth();
            }
            //pocita sa tam aj s tym ze ak sa resizne obrazok je potrebne upravit polohu BB
            Image gtImg, algImg;
            if (toggleButtonBoundingBoxInFrame.isSelected()) {
                algImg = ImageFactory.getImageWithBoundingBoxes(path, gtBoxes, algBoxes, Color.RED, Color.BLUE, labelWidth, labelHeight);
            } else {
                algImg = ImageFactory.getImageWithBoundingBoxes(path, algBoxes, Color.BLUE, labelWidth, labelHeight);
            }
            gtImg = ImageFactory.getImageWithBoundingBoxes(path, gtBoxes, Color.RED, labelWidth, labelHeight);
            gtImageLabel.setIcon(new ImageIcon(gtImg));
            algImageLabel.setIcon(new ImageIcon(algImg));
            //updatnut x,y,w,h , 0 bounding box
            inputAlgX.setText(String.valueOf(algBoxes[0].getxLeftUpper()));
            inputAlgY.setText(String.valueOf(algBoxes[0].getyLeftUpper()));
            inputAlgWidth.setText(String.valueOf(algBoxes[0].getWidth()));
            inputAlgHeight.setText(String.valueOf(algBoxes[0].getHeight()));

            inputGroundTruthX.setText(String.valueOf(gtBoxes[0].getxLeftUpper()));
            inputGroundTruthY.setText(String.valueOf(gtBoxes[0].getyLeftUpper()));
            inputGroundTruthWidth.setText(String.valueOf(gtBoxes[0].getWidth()));
            inputGroundTruthHeight.setText(String.valueOf(gtBoxes[0].getHeight()));
            this.updateMetricsValues();
        }
    }

    private void updateSubsequentValues() {
        //ziskat cisla
        try {
            // -1 kvoli indexovaniu od 0
            int from = Integer.parseInt(inputSubSeqFrom.getText()) - 1;
            int to = Integer.parseInt(inputSubSeqTo.getText()) - 1;
            if (metrics != null) {
                DecimalFormat decimalFormat = new DecimalFormat("##.##");
                //f measure
                MetricFrame[] metF = metrics.getFmeasure(from, to);
                double value = metrics.getAvgMetricValue(metF);
                inputFMeasureSub.setText(decimalFormat.format(value));
                //intersection over union
                metF = metrics.getInt_over_union(from, to);
                value = metrics.getAvgMetricValue(metF);
                inputIntersectionSub.setText(decimalFormat.format(value));
                //precision
                metF = metrics.getPrecision(from, to);
                value = metrics.getAvgMetricValue(metF);
                inputPrecisionSub.setText(decimalFormat.format(value));
                //recall
                metF = metrics.getRecall(from, to);
                value = metrics.getAvgMetricValue(metF);
                inputRecallSub.setText(decimalFormat.format(value));
            }
        } catch (NumberFormatException ex) {
            printErrorMsg("Number format error.");
        }
    }

    private void updateMetricsValues() {
        //nvm ci taky pripad moze nastat, zatial s if
        if (metrics != null) {
            //zatial vsade 0 index
            int INDEX = 0;
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            //f measure
            MetricFrame[] metF = metrics.getFmeasure();
            double value = metF[actualFrameIndex].getMetricValueForObjects()[INDEX];
            inputFMeasureActual.setText(decimalFormat.format(value));
            //intersection over union
            metF = metrics.getInt_over_union();
            value = metF[actualFrameIndex].getMetricValueForObjects()[INDEX];
            inputIntersectionActual.setText(decimalFormat.format(value));
            //precision
            metF = metrics.getPrecision();
            value = metF[actualFrameIndex].getMetricValueForObjects()[INDEX];
            inputPrecisionActual.setText(decimalFormat.format(value));
            //recall
            metF = metrics.getRecall();
            value = metF[actualFrameIndex].getMetricValueForObjects()[INDEX];
            inputRecallActual.setText(decimalFormat.format(value));
        }
    }

    private void recalculateImageSize() {
        if (gtImageLabel.getIcon() == null || algImageLabel.getIcon() == null) {
            return;
        }
        //zistit ci velkost obrazka je rovnaka ako velkost panelu
        int labelW = gtImageLabel.getWidth();
        int labelH = gtImageLabel.getHeight();
        int imgW = gtImageLabel.getIcon().getIconWidth();
        int imgH = gtImageLabel.getIcon().getIconHeight();
        int w = labelW - gtImageLabel.getInsets().right - gtImageLabel.getInsets().right;
        int h = labelH - gtImageLabel.getInsets().top - gtImageLabel.getInsets().bottom;
        if (imgW != w || imgH != h) {
            Image newImg = ((ImageIcon) gtImageLabel.getIcon()).getImage().getScaledInstance(w, h, Image.SCALE_DEFAULT);
            gtImageLabel.setIcon(new ImageIcon(newImg));
            algImageLabel.setIcon(new ImageIcon(newImg));
        }

    }

    private void showChart() {
        String selectedMetric = (String) comboBoxMetrics.getSelectedItem();
        try {
            // -1 kvoli indexovaniu od 0
            int rangeFromNum = Integer.parseInt(inputSubSeqFrom.getText()) - 1;
            int rangeToNum = Integer.parseInt(inputSubSeqTo.getText()) - 1;

            MetricFrame[] metricArraySub;
            switch (selectedMetric) {
                case "Precision":
                    metricArraySub = metrics.getPrecision(rangeFromNum, rangeToNum);
                    break;
                case "Recall":
                    metricArraySub = metrics.getRecall(rangeFromNum, rangeToNum);
                    break;
                case "Intersection over union":
                    metricArraySub = metrics.getInt_over_union(rangeFromNum, rangeToNum);
                    break;
                default:
                    metricArraySub = metrics.getFmeasure(rangeFromNum, rangeToNum);
                    break;
            }

            if (metricArraySub != null) {
                ChartWindow chWindow = new ChartWindow(selectedMetric, rangeFromNum, rangeToNum);
                chWindow.addDataset(metricArraySub);
                chWindow.chartMain();
                chWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                chWindow.pack();
                chWindow.setVisible(true);
            } else {
                printErrorMsg("Wrong indexes");
            }
        } catch (NumberFormatException ex) {
            printErrorMsg("Wrong index format");
        }

    }

    private void moveSequenceLeft() {
        if (actualFrameIndex > 0) {
            actualFrameIndex--;
            listSequenceImages.setSelectedIndex(actualFrameIndex);
        }
    }

    private void startSequence() {
        if (listViews == null || listViews.size() == 0) {
            printErrorMsg("There is no sequence.");
            return;
        }
        if (!sequencePlaying) {
            sequencePlaying = true;
            threadSequence = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (actualFrameIndex < listViews.size() - 1 && sequencePlaying) {
                        try {
                            double delay = (sliderSpeed.getMaximum() - sliderSpeed.getValue()) * 0.5 + 0.5;
                            Thread.currentThread().sleep((int) (delay * 1000));
                            Window.this.moveSequenceRight();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Window.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    sequencePlaying = false;
                }
            });
            threadSequence.start();
        }
    }

    private void stopSequence() {
        if (threadSequence != null) {
            sequencePlaying = false;
        }
    }

    private void printErrorMsg(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    private void moveSequenceRight() {
        if (actualFrameIndex < listViews.size() - 1) {
            actualFrameIndex++;
            listSequenceImages.setSelectedIndex(actualFrameIndex);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(buttonGraph)) {
            showChart();
        } else if (e.getSource().equals(buttonMoveLeft)) {
            moveSequenceLeft();
        } else if (e.getSource().equals(buttonMoveRight)) {
            moveSequenceRight();
        } else if (e.getSource().equals(buttonCalculateSubSeq)) {
            updateSubsequentValues();
        } else if (e.getSource().equals(buttonStartSequence)) {
            startSequence();
        } else if (e.getSource().equals(buttonStopSequence)) {
            stopSequence();
        }

    }

    @Override
    public void componentResized(ComponentEvent e) {
        recalculateImageSize();
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        wheelRotation += Math.abs(e.getWheelRotation());
        if(Math.abs(wheelRotation) < 2)
            return;
        if(e.getSource().equals(gtImageLabel))
            ImageFactory.zoomImage(gtImageLabel,e.getX(),e.getY(),wheelRotation,EnumIconType.GROUND_TRUTH);
        else if(e.getSource().equals(algImageLabel))
            ImageFactory.zoomImage(algImageLabel,e.getX(),e.getY(),wheelRotation,EnumIconType.ALGORITHM);
        wheelRotation = 0;

    }

    private void exportToFile() {
        if(metrics == null){
            printErrorMsg("Nothing to save");
            return;
        }
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        int result = fileChooser.showSaveDialog(frame);
        if(result == JFileChooser.APPROVE_OPTION){
            String fileName = fileChooser.getSelectedFile().getPath() + ".xml";
            metrics.exportToFile(new File(fileName));
        }
    }

    
    private void initMouseAdapter(){
        MouseInputListener list = new MouseInputListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                
               
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getSource().equals(gtImageLabel)){
                    xGTLabel = e.getX();
                    yGTLabel = e.getY();
                }else if(e.getSource().equals(algImageLabel)){
                    xAlgLabel = e.getX();
                    yAlgLabel = e.getY();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(e.getSource().equals(gtImageLabel)){
                    ImageFactory.moveImage(gtImageLabel, xGTLabel, yGTLabel, xDragGTLabel, yDragGTLabel, EnumIconType.GROUND_TRUTH);
                }else if(e.getSource().equals(algImageLabel)){
                    ImageFactory.moveImage(algImageLabel, xAlgLabel, yAlgLabel, xDragAlgLabel, yDragAlgLabel, EnumIconType.ALGORITHM);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
               
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if(e.getSource().equals(gtImageLabel)){
                    
                }else if(e.getSource().equals(algImageLabel)){
                    
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if(e.getSource().equals(gtImageLabel)){
                    xDragGTLabel = e.getX();
                    yDragGTLabel = e.getY();
                }else if(e.getSource().equals(algImageLabel)){
                    xDragAlgLabel = e.getX();
                    yDragAlgLabel = e.getY();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                
            }
        };
        algImageLabel.addMouseListener(list);
        algImageLabel.addMouseMotionListener(list);
        gtImageLabel.addMouseListener(list);
        gtImageLabel.addMouseMotionListener(list);
    }

}
