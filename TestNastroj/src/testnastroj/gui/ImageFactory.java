/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import testnastroj.enums.EnumIconType;
import testnastroj.object.BoundingBox;

/**
 *
 * @author 
 */
public class ImageFactory {
    /**
     * Vsetky podporovane obrazkove pripony.
     * Mozno neskor nacitavanie pripon zo suboru.
     */
    public static String[] IMAGE_EXTENSIONS = new String[]{"png","jpg","jpeg"};
    private static float THICKNESS = 3;
    
    private static BufferedImage oldGT;
    private static BufferedImage oldAlg;
    
    private static int xGT;
    private static int yGT;
    private static int xAlg;
    private static int yAlg;
    
    /**
     * Vytvori novy obrazok s dokreslenymi boundind boxmi.
     * @param sourcePath povodny obrazok cesta
     * @param boxes bounding box
     * @param color farba 
     * @param width nova sirka
     * @param height nova vyska
     * @return 
     */
    public static Image getImageWithBoundingBoxes(String sourcePath,BoundingBox[] boxes,Color color,int width,int height){
        
        try {
            BufferedImage source = ImageIO.read(new File(sourcePath));
            Image image;
            double scaleRatioH = 1;
            double scaleRatioW = 1;
            if(source.getWidth() != width || source.getHeight() != height){
                image = source.getScaledInstance(width, height, Image.SCALE_DEFAULT);
                scaleRatioH = (double)height/source.getHeight();
                scaleRatioW = (double)width/source.getWidth();
            }
            else
                image = source;
            
            BufferedImage newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphic = newImg.createGraphics();
            Stroke old = graphic.getStroke();
            graphic.setStroke(new BasicStroke(THICKNESS));
            //nakreslit povodny obrazok
            graphic.drawImage(image, 0, 0, null);
            for (BoundingBox box : boxes) {
                graphic.setColor(color);
                int x = (int)(box.getxLeftUpper() * scaleRatioW);
                int y = (int)(box.getyLeftUpper() * scaleRatioH);
                int w = (int)(box.getWidth() * scaleRatioW);
                int h = (int)(box.getHeight() * scaleRatioH);
                graphic.drawRect(x, y, w, h);
            }
            graphic.setStroke(old);
            graphic.dispose();
            return newImg;
        } catch (IOException ex) {
            Logger.getLogger(ImageFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
     public static Image getImageWithBoundingBoxes(String sourcePath,BoundingBox[] boxesGt,BoundingBox[] boxesAlg,Color colorGT,Color colorAlg,int width,int height){
        
        try {
            BufferedImage source = ImageIO.read(new File(sourcePath));
            Image image;
            double scaleRatioH = 1;
            double scaleRatioW = 1;
            if(source.getWidth() != width || source.getHeight() != height){
                image = source.getScaledInstance(width, height, Image.SCALE_DEFAULT);
                scaleRatioH = (double)height/source.getHeight();
                scaleRatioW = (double)width/source.getWidth();
            }
            else
                image = source;
            
            BufferedImage newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphic = newImg.createGraphics();
            Stroke old = graphic.getStroke();
            graphic.setStroke(new BasicStroke(THICKNESS));
            //nakreslit povodny obrazok
            graphic.drawImage(image, 0, 0, null);
            for (BoundingBox box : boxesGt) {
                graphic.setColor(colorGT);
                int x = (int)(box.getxLeftUpper() * scaleRatioW);
                int y = (int)(box.getyLeftUpper() * scaleRatioH);
                int w = (int)(box.getWidth() * scaleRatioW);
                int h = (int)(box.getHeight() * scaleRatioH);
                graphic.drawRect(x, y, w, h);
            }
            for (BoundingBox box : boxesAlg) {
                graphic.setColor(colorAlg);
                int x = (int)(box.getxLeftUpper() * scaleRatioW);
                int y = (int)(box.getyLeftUpper() * scaleRatioH);
                int w = (int)(box.getWidth() * scaleRatioW);
                int h = (int)(box.getHeight() * scaleRatioH);
                graphic.drawRect(x, y, w, h);
            }
            graphic.setStroke(old);
            graphic.dispose();
            return newImg;
        } catch (IOException ex) {
            Logger.getLogger(ImageFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void zoomImage(JLabel label,int x,int y,int rotation,EnumIconType type) {
        if(label != null && label.getIcon() != null){
            ImageIcon icon = (ImageIcon)label.getIcon();
            Image image = icon.getImage();
            int oldWidth = icon.getIconWidth();
            int oldHeight = icon.getIconHeight();
            //o 5% zvacsi zmensi za 1 rotaciu
            int newWidth = (int)(icon.getIconWidth() + 0.05*rotation*icon.getIconWidth());
            int newHeight= (int)(icon.getIconHeight()+ 0.05*rotation*icon.getIconHeight());
            
            Image scaledImage;
            if(type == EnumIconType.ALGORITHM){
                if(oldAlg == null)
                    scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT); 
                else{
                    newWidth = (int)(oldAlg.getWidth() + 0.05*rotation*icon.getIconWidth());
                    newHeight= (int)(oldAlg.getHeight() + 0.05*rotation*icon.getIconHeight());
                    scaledImage = oldAlg.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT); 
                }
                oldAlg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
                Graphics2D bGr = oldAlg.createGraphics();
                bGr.drawImage(scaledImage, 0, 0, null);
                bGr.dispose();
                xAlg = 0; yAlg = 0;
            }
            else{
                if(oldGT == null)
                    scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT); 
                else{
                    newWidth = (int)(oldGT.getWidth() + 0.05*rotation*icon.getIconWidth());
                    newHeight= (int)(oldGT.getHeight() + 0.05*rotation*icon.getIconHeight());
                    scaledImage = oldGT.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT); 
                }
                oldGT= new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
                Graphics2D bGr = oldGT.createGraphics();
                bGr.drawImage(scaledImage, 0, 0, null);
                bGr.dispose();
                xGT = 0; yGT = 0;
            }
            //crop
            BufferedImage buffered; 
            if(rotation > 0)
                buffered = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
            else
                 buffered = new BufferedImage(oldWidth, oldHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D bGr = buffered.createGraphics();
            bGr.drawImage(scaledImage, 0, 0, null);
            bGr.dispose();
            System.out.println("BFIMG w a h=" + buffered.getWidth() + " " + buffered.getHeight());
            System.out.println("old w a h=" + oldWidth + " " + oldHeight);
            System.out.println("NEW w a h=" + newWidth + " " + newHeight);
            buffered = buffered.getSubimage(0, 0, oldWidth, oldHeight);
            label.setIcon(new ImageIcon(buffered));
            
        } 
    }   
    
    public static void moveImage(JLabel label,int x,int y,int newX, int newY,EnumIconType type){
        Icon icon = label.getIcon();
        if(icon == null)
            return;
        int deltaX = newX - x;
        int deltaY = newY - y;
        
        BufferedImage image;
        int oldX,oldY;
        if(type == EnumIconType.ALGORITHM ){
            image = oldAlg;
            oldX = xAlg;
            oldY = yAlg;
        }
        else{
            image = oldGT;
            oldX = xGT;
            oldY = yGT;
        }
        if(image == null)
            return;
        if(oldX + deltaX < 0 || oldX + deltaX + icon.getIconWidth() > image.getWidth())
            return;
        if(oldY + deltaY < 0 || oldY + deltaY + icon.getIconHeight() > image.getHeight())
            return;
        int imageX = oldX + deltaX;
        int imageY = oldY + deltaY;
        if(type == EnumIconType.ALGORITHM){
            xAlg = imageX;
            yAlg = imageY;
        }else{
            xGT = imageX;
            yGT = imageY;
        }
        BufferedImage buffered = image.getSubimage(imageX, imageY,icon.getIconWidth() ,icon.getIconHeight());
        label.setIcon(new ImageIcon(buffered));
    }
}
