/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.gui;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author SaL
 */
public class ImageView{
    private final String imagePath;
    
    private  int width;
    private  int height;

    public ImageView(int width,int height,String imagePath) {
       
        this.imagePath = imagePath;
        this.width = width;
        this.height = height;
    }
    
    public void setImageSize(int width,int height){
        this.width = width;
        this.height = height;
    }
    
    private Image getImage(){
        Image image = null;
        try {
            image = ImageIO.read(new File(imagePath));
        } catch (IOException ex) {
            //print error
            Logger.getLogger(ImageView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }  
    
    public ImageIcon getIcon(){
        //resize image
        Image image = getImage();
        if(image == null)
            return null;
        Image resized = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        return new ImageIcon(resized);
    }

    public String getImagePath() {
        return imagePath;
    }
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }
    
    
}
