package testnastroj.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import testnastroj.metrics.MetricFrame;

public class ChartWindow extends JFrame{
    private XYSeriesCollection result;
    private int firstImage;
    private int lastImage;
    private String chartType;
        
    public ChartWindow(String chartType, int first, int last) {
        super("Chart");
        this.result = new XYSeriesCollection();
        this.firstImage = first;
        this.chartType = chartType;
        this.lastImage = last;
    }
    
    public void addDataset(MetricFrame[] values){
        double frameSum, frameObjects;
        
        XYSeries series1 = new XYSeries("Metrics for subsequence: "+(firstImage+1)+"-"+lastImage);
        for (int i = firstImage; i < values.length; i++) {
            frameSum = 0;
            frameObjects = values[i].getMetricValueForObjects().length;
            for (int j = 0; j < frameObjects; j++) {
                frameSum += values[i].getMetricValueForObjects()[j];
            }
            series1.add(i+1, frameSum/frameObjects);
        }
        result.addSeries(series1);
    }
    
    private  XYDataset createDataset() {
        return result;
    }
    
    private JFreeChart createChart(XYDataset dataset) {

        String chartTitle = chartType;
        String xAxisLabel = "Frame";
        String yAxisLabel = "Value";
        
        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle,
            xAxisLabel, yAxisLabel, dataset);
        
        XYPlot plot = chart.getXYPlot();
        plot.setRangePannable(true);
        plot.setDomainPannable(true);
        
        ValueAxis domainAxis = plot.getDomainAxis();
        domainAxis.setRange(firstImage, lastImage+1);
        domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        plot.setOutlinePaint(Color.BLACK);
        plot.setOutlineStroke(new BasicStroke(2.0f));
        
        plot.setBackgroundPaint(Color.DARK_GRAY);
        plot.setRangeGridlinesVisible(true);
        
        plot.setRangeGridlinePaint(Color.BLACK);
        plot.setDomainGridlinesVisible(true);
        
        plot.setDomainGridlinePaint(Color.BLACK);
        
        final Marker start = new ValueMarker(0.50);
        start.setPaint(Color.yellow);        
        plot.addRangeMarker(start);
        
        
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        //renderer.setSeriesPaint(0,Color.yellow);
        plot.setRenderer(renderer);
        
        return chart;
    }
    
    public void chartMain(){
        XYDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);        
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1000, 540));
        setContentPane(chartPanel);
    }
}

