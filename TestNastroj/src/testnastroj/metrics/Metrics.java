/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.metrics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import testnastroj.object.BoundingBox;
import testnastroj.object.FoundObject;
import testnastroj.object.Frame;

/**
 *
 * @author Juraj
 */
public class Metrics {

    private Frame[] frames;

    MetricFrame[] precision;
    MetricFrame[] recall;
    MetricFrame[] fmeasure;
    MetricFrame[] int_over_union;

    public Metrics(Frame[] frames) {
        this.frames = frames;
        this.calculatePrecision();
        this.calculateRecall();
        this.calculateFmeasure();
        this.calculateInt_over_union();
    }
    
    

    private void calculatePrecision() {
        precision = new MetricFrame[frames.length];
        for (int i = 0; i < frames.length; i++) {
            FoundObject[] objectsInFrame = frames[i].getFoundObjects();
            double[] metricsValsForFrameObjects = new double[objectsInFrame.length];
            for (int j = 0; j < objectsInFrame.length; j++) {
                BoundingBox gt = objectsInFrame[j].getGroundTruth();
                BoundingBox algo = objectsInFrame[j].getAlgoValues();
                double tp = gt.getOverlapingArea(algo);
                double fp = algo.getArea() - tp;
                metricsValsForFrameObjects[j] =  tp / (tp + fp);
                //System.out.println("p: " + metricsValsForFrameObjects[j]);
            }
            precision[i] = new MetricFrame(metricsValsForFrameObjects);
        }        
    }

    private void calculateRecall() {
        recall = new MetricFrame[frames.length];
        for (int i = 0; i < frames.length; i++) {
            FoundObject[] objectsInFrame = frames[i].getFoundObjects();
            double[] metricsValsForFrameObjects = new double[objectsInFrame.length];
            for (int j = 0; j < objectsInFrame.length; j++) {
                BoundingBox gt = objectsInFrame[j].getGroundTruth();
                BoundingBox algo = objectsInFrame[j].getAlgoValues();
                double tp = gt.getOverlapingArea(algo);
                double fn = gt.getArea() - tp;
                metricsValsForFrameObjects[j] = tp / (tp + fn);
                //System.out.println("r: " + metricsValsForFrameObjects[j]);
            }
            recall[i] = new MetricFrame(metricsValsForFrameObjects);
        }
    }

    private void calculateFmeasure() {
        fmeasure = new MetricFrame[frames.length];
        for (int i = 0; i < frames.length; i++) {
            FoundObject[] objectsInFrame = frames[i].getFoundObjects();
            double[] metricsValsForFrameObjects = new double[objectsInFrame.length];
            for (int j = 0; j < objectsInFrame.length; j++) {
                BoundingBox gt = objectsInFrame[j].getGroundTruth();
                BoundingBox algo = objectsInFrame[j].getAlgoValues();
                double prec = precision[i].getMetricValueForObjects()[j];
                double rec = recall[i].getMetricValueForObjects()[j];    
                double result = (prec == 0 || rec == 0) ? 0 : (2 * (prec * rec) / (prec + rec));
                metricsValsForFrameObjects[j] = result;
                //System.out.println("f: " + metricsValsForFrameObjects[j]);
            }
            fmeasure[i] = new MetricFrame(metricsValsForFrameObjects);
        }
    }

    private void calculateInt_over_union() {
        int_over_union = new MetricFrame[frames.length];
        for (int i = 0; i < frames.length; i++) {
            FoundObject[] objectsInFrame = frames[i].getFoundObjects();
            double[] metricsValsForFrameObjects = new double[objectsInFrame.length];
            for (int j = 0; j < objectsInFrame.length; j++) {
                BoundingBox gt = objectsInFrame[j].getGroundTruth();
                BoundingBox algo = objectsInFrame[j].getAlgoValues();
                double intersect = gt.getOverlapingArea(algo);
                double union = gt.getArea() + algo.getArea() - intersect;
                metricsValsForFrameObjects[j] = intersect / union;
                //System.out.println("iou: " + metricsValsForFrameObjects[j]);
            }
            int_over_union[i] = new MetricFrame(metricsValsForFrameObjects);
        }
    }

    public MetricFrame[] getPrecision() {
        return precision;
    }

    public MetricFrame[] getRecall() {
        return recall;
    }

    public MetricFrame[] getFmeasure() {
        return fmeasure;
    }

    public MetricFrame[] getInt_over_union() {
        return int_over_union;
    }
    
    /**
     * vrati podsekvenciu pre prislusnu metriku
     * @param from
     * @param to
     * @return 
     */
    public MetricFrame[] getPrecision(int from, int to) {
        boolean invalid = (from < 0 || to < 0 || from > to || 
                from >= frames.length || to >= frames.length);
        return invalid ? null : Arrays.copyOfRange(precision, from, to);
    }

    public MetricFrame[] getRecall(int from, int to) {
        boolean invalid = (from < 0 || to < 0 || from > to || 
                from >= frames.length || to >= frames.length);
        return invalid ? null : Arrays.copyOfRange(recall, from, to);
    }

    public MetricFrame[] getFmeasure(int from, int to) {
        boolean invalid = (from < 0 || to < 0 || from > to || 
                from >= frames.length || to >= frames.length);
        return invalid ? null : Arrays.copyOfRange(fmeasure, from, to);
    }

    public MetricFrame[] getInt_over_union(int from, int to) {
        boolean invalid = (from < 0 || to < 0 || from > to || 
                from >= frames.length || to >= frames.length);
        return invalid ? null : Arrays.copyOfRange(int_over_union, from, to);
    }
    
    
    /**
     * vypocita priemernu hodnotu metriky subsekvencie
     * @param metric
     * @return 
     */
    public double getAvgMetricValue(MetricFrame[] metric){
        double sum = 0;
        for (int i = 0; i < metric.length; i++) {
            double frameSum = 0;
            double[] frameMetrics = metric[i].getMetricValueForObjects();
            for (int j = 0; j < frameMetrics.length; j++) {
                frameSum += frameMetrics[j];
            }
            sum += frameSum / frameMetrics.length;
        } 
        return sum / metric.length;
    }
    
    /**
     * vypocita priemernu hodnotu metriky - priemernu hodnotu metriky pre snimok
     * @param metric
     * @return 
     */
    public double getAvgMetricValue(MetricFrame metric){
        double sum = 0;
        double[] metricVals = metric.getMetricValueForObjects();
        for (int i = 0; i < metricVals.length; i++) {
            sum += metricVals[i];
        }
        return sum / metricVals.length;
    } 
    
    /**
    * exportuje hodnoty metrik pre jednotlive snimky do xml suboru v parametri    * 
     * @param file kam exportovat
    */
    public void exportToFile(File file){
        try {
            PrintWriter pw = new PrintWriter(file);
            pw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + "<data>\n" + "\timages\n");
            for (int i = 0; i < frames.length; i++) {
                pw.write("\t\t<image>\n");
                pw.write("\t\t\t<metrics>\n");
                pw.write("\t\t\t\t<precision>" + 
                        String.format("%.2f", this.getAvgMetricValue(precision[i])) + "</precision>\n");
                pw.write("\t\t\t\t<recall>" + 
                        String.format("%.2f", this.getAvgMetricValue(recall[i])) + "</recall>\n");
                pw.write("\t\t\t\t<fmeasure>" + 
                        String.format("%.2f", this.getAvgMetricValue(fmeasure[i])) + "</fmeasure>\n");
                pw.write("\t\t\t\t<int_over_union>" + 
                        String.format("%.2f", this.getAvgMetricValue(int_over_union[i])) + "</int_over_union>\n");
                pw.write("\t\t\t</metrics>\n");
                pw.write("\t\t</image>\n");                
            }
            pw.write("\t</images>\n");
            pw.write("</data\n");
            pw.close();
        } catch (FileNotFoundException ex) {
        }
        
    }
    
    
}
