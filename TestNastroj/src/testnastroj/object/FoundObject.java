/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.object;

/**
 *
 * @author SaL
 */
public class FoundObject {
    
    private BoundingBox groundTruth;
    private BoundingBox algoValues;
    private String objectClassName;   
    private ObjectAttribute[] objectAttributes; 
    private int trackId = 0; //defaultne 0, nemusi byt zadana pri single objekte
    
           

    public FoundObject(String objectClassName, ObjectAttribute[] objectAttributes) {
        this.objectClassName = objectClassName;
        this.objectAttributes = objectAttributes;
    }
    
    //pretazeny konstruktor pre trackId
    public FoundObject(int trackId, String objectClassName, ObjectAttribute[] objectAttributes) {
        this.trackId = trackId;
        this.objectClassName = objectClassName;
        this.objectAttributes = objectAttributes;
    }
    

    public void setGroundTruth(BoundingBox groundTruth) {
        this.groundTruth = groundTruth;
    }

    public void setAlgoValues(BoundingBox algoValues) {
        this.algoValues = algoValues;
    }

    public BoundingBox getGroundTruth() {
        return groundTruth;
    }

    public BoundingBox getAlgoValues() {
        return algoValues;
    }

    public int getTrackId() {
        return trackId;
    }    

}
