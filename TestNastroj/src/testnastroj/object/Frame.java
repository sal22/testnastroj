/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.object;

/**
 *
 * @author Juraj
 */
public class Frame {
    
    String imageSourceFile; //relativna cesta
    FoundObject[] foundObjects;

    public Frame(String imageSourceFile, FoundObject[] foundObjects) {
        this.imageSourceFile = imageSourceFile;
        this.foundObjects = foundObjects;
    }

    public String getImageSourceFile() {
        return imageSourceFile;
    }

    public FoundObject[] getFoundObjects() {
        return foundObjects;
    }
    
    
    
}
