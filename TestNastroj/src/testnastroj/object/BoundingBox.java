/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testnastroj.object;

import java.awt.Rectangle;

/**
 *
 * @author Juraj
 */
public class BoundingBox {
    
    private int xLeftUpper;
    private int yLeftUpper;
    private int width;
    private int height;

    public BoundingBox(int xLeftUpper, int yLeftUpper, int width, int height) {
        this.xLeftUpper = xLeftUpper;
        this.yLeftUpper = yLeftUpper;
        this.width = width;
        this.height = height;
    }

    public int getxLeftUpper() {
        return xLeftUpper;
    }

    public int getyLeftUpper() {
        return yLeftUpper;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getArea() {
        return this.getWidth() * this.getHeight();
    }

    /**
     * pre 2 bounding boxy vypocita ich prienik
     * suradnice x1, y1 su lavy dolny roh, x2, y2 su pravy horny
     * @param bb box2
     * @return
     */
    public int getOverlapingArea(BoundingBox bb) {
        Rectangle rect1 = new Rectangle(xLeftUpper, yLeftUpper, width, height);
        Rectangle rect2 = new Rectangle(bb.getxLeftUpper(), bb.yLeftUpper, bb.getWidth(), bb.getHeight());
        Rectangle intersection = rect1.intersection(rect2);
        if (intersection.height <= 0 || intersection.width <= 0) {
            return 0;
        }
        return (int) (intersection.height * intersection.width);
    }
    
    
    
    
}
